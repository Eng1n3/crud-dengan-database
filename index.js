const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const methodOverride = require('method-override')
const port = 3000;


// config method-override HTTP
app.use(methodOverride('_method'))

// validasi
const { body, validationResult, check } = require('express-validator');

// Config database
require('./utils/db');
const Contact = require('./model/contact');
const ObjectID = require('mongoose').ObjectID;

//untuk config template
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(express.static(path.join(__dirname + '/public')));
app.use(express.urlencoded({extended: true}));

// Untuk config flash
const flash = require('connect-flash');
app.use(cookieParser('secret'));
app.use(session({
    secret: 'secret',
    maxAge: 3600,
    resave: true,
    saveUninitialized: true
}));
app.use(flash());

app.get('/', (req, res) => {
    res.render('index', {
        layout: 'layouts/base',
        title: 'Home'
    })
})

app.get('/about', (req, res) => {
    res.render('about', {
        layout: 'layouts/base',
        title: 'About'
    });
});

app.get('/contact', async (req, res) => {
    const dataContact = await Contact.find();
    res.render('contact', {
        layout: './layouts/base',
        title: 'Contact',
        msg: req.flash('msg'),
        dataContact
    })
})

app.post('/contact', 
    [
        check('email', 'Email tidak valid!').isEmail(),
        body('email').custom( async (value) => {
            const duplikat = await Contact.findOne({email: value});
            if ( duplikat ) {
                throw new Error('Email sudah digunakan!');
            }
            return true;
        }),
        check('nomor', 'Nomor tidak valid!').isMobilePhone('id-ID'),
        body('nomor').custom( async (value) => {
            const duplikat = await Contact.findOne({nomor: value});
            if ( duplikat ) {
                throw new Error('Nomor sudah digunakan!');
            }
            return true;
        })
    ],
    (req, res) => {
        const errors = validationResult(req);
        if ( !errors.isEmpty() ) {
            res.render('create-data', {
                layout: './layouts/base',
                title: 'Create Data',
                errors: errors.array()
            })
        } else {
            Contact.insertMany(req.body, (error, result) => {
                req.flash('msg', 'Data berhasil ditambahkan');
                res.redirect('./contact');
            })
        }
})

app.put('/contact', 
    [
        check('email', 'Email tidak valid!').isEmail(),
        body('email').custom( async (value, {req}) => {
            const duplikat = await Contact.find({
                _id: { $ne: req.body.id},
                email: value
            });
            if (duplikat.length !== 0) {
                throw new Error("Email sudah digunakan");
            }
            return true;
        }),
        check('nomor', 'Nomor tidak valid!').isMobilePhone('id-ID'),
        body('nomor').custom( async (value, {req}) => {
            const duplikat = await Contact.find({
                _id: { $ne: req.body.id },
                nomor: value
            });
            if ( duplikat.length !== 0 ) {
                throw new Error('Nomor sudah digunakan!');
            }
            return true;
        })
    ],
    (req, res) => {
        const errors = validationResult(req);
        if ( !errors.isEmpty() ) {
            res.render('update-data', {
                layout: './layouts/base',
                title: 'Update Data',
                errors: errors.array(),
                contact: req.body
            })
        } else {
            Contact.updateOne(
                {
                    _id: req.body.id
                },
                {
                    $set: {
                        nama: req.body.nama,
                        email: req.body.email,
                        nomor: req.body.nomor
                    }
                }
            ).then(result => {
                req.flash('msg', 'Data berhasil diupdate');
                res.redirect('./contact');
            })
        }
})

app.delete('/contact', (req, res) => {
    const idContact = req.body.id;
    Contact.deleteOne({_id: idContact}).then( result => {
        req.flash('msg', 'Data berhasil dihapus!');
        res.redirect('/contact');
    }).catch(error => console.log(error));
})

app.get('/contact/create/contact', (req, res) => {
    res.render('create-data', {
        layout: './layouts/base',
        title: 'Create Contact'
    });
})

app.get('/contact/update/:id', async(req, res) => {
    const idContact = req.params.id;
    const contact = await Contact.findOne({_id: idContact});
    res.render('update-data', {
        layout: './layouts/base',
        title: 'update data',
        contact
    })
})

app.get('/contact/:id', async (req, res) => {
    const idContact = req.params.id;
    const contact = await Contact.findOne({_id: idContact});
    res.render('detail', {
        layout: './layouts/base',
        title: 'Contact',
        contact
    })
})

app.use('/', (req, res) => {
    res.status(404);
    res.send('<h1>404 Not found</h1>');
})

app.listen(port, () => console.log(`Server is listen at http://localhost:${port}`));